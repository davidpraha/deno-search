import { serve } from "https://deno.land/std@0.61.0/http/server.ts";
import { multiParserV2, FormV2, FormFileV2 } from 'https://deno.land/x/multiparser@v2.0.1/mod.ts'

interface Form {
    fields: Record<string, string>;
    files: Record<string, FormFileV2 | FormFileV2[]>;
  }

const s = serve({ port: 8000 });
console.log("Serve on port 8000");

for await (const req of s) {
  if (req.url === "/upload") {
    const form = await multiParserV2(req);
      //var uint8array = new TextEncoder().encode("Plain Text");
      //var string = new TextDecoder().decode(uint8array);
      //console.log(uint8array, string);
      //console.log(form.fields.singleStr);
      console.log(form);
    }
  req.respond({
    headers: new Headers({ "Content-Type": "text/html; charset=utf-8" }),
    body: `
    <h3>Deno http module</h3>
    <form action="/upload" enctype="multipart/form-data" method="post">
      <div>singleStr: <input type="text" name="singleStr" /></div>
      <div>singleImg: <input type="file" name="singleImg"/></div>
      <input type="submit" value="Upload" />
    </form>
  ` });
}